<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"><?php echo $page_title;?></h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>User Email</th>
                            <th>Package Name</th>
                            <th>Package Price</th>
                            <th>Number of Package</th>
                            <th>Total Amount</th>
                            <th>Store Amount</th>
                            <th>Payment Type</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $sl = 1;
                            foreach ($subscriptions as $subscription):                     
                        ?>
                        <tr>
                            <td><?php echo $sl++;?></td>
                            <td><?php echo $subscription['name']; ?></td>
                            <td><?php echo $subscription['email']; ?></td>
                            <td><?php echo $subscription['package_name']; ?></td>
                            <td><?php echo $subscription['package_price']; ?></td>
                            <td><?php echo $subscription['num_of_item']; ?></td>
                            <td><?php echo $subscription['total_amount']; ?></td>
                            <td><?php echo $subscription['store_amount']; ?></td>
                            <td><?php echo $subscription['card_type']; ?></td>
                            <td><?php echo $subscription['start_date']; ?></td>
                            <td><?php echo $subscription['end_date']; ?></td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
                <?php echo $links; ?>
            </div>
        </div>
    </div>
</div>

    <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/parsleyjs/dist/parsley.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('form').parsley();
        });
    </script>

    <!-- select2-->
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js" type="text/javascript"></script>
    <!-- select2-->