<?php 
    $packages    = $this->db->get_where('package', array('package_id' => $param2))->result_array();
    foreach ($packages as $row) :
    echo form_open(base_url() . 'admin/packages/update/'.$param2, array('class' => 'form-horizontal group-border-dashed', 'enctype' => 'multipart/form-data'));
?>
<!-- modal header -->
<div class="modal-header">
  <h5 class="modal-title" id="exampleModalLabel">Update Package</h5>
  <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
</div>

<!-- modal body -->
<div class="modal-body">  
  <div class="form-group">
    <label class="control-label">Package Name</label>
    <input type="text" name="name" value="<?php echo $row['name']; ?>" class="form-control" placeholder="Enter Package Name" />
  </div>
  <div class="form-group">
    <label class="control-label">Price</label>
    <input type="number" name="price" value="<?php echo $row['price']; ?>" class="form-control" required placeholder="Enter Package Price" />
  </div>
  <div class="form-group">
    <label class="control-label">Number of Participant</label>
    <input type="number" name="participant" value="<?php echo $row['participant']; ?>" class="form-control" placeholder="Enter Participant Name" />
  </div>
   <div class="form-group">
    <label class="control-label">Duration</label>
    <input type="number" name="duration" value="<?php echo $row['duration']; ?>" class="form-control" placeholder="Enter Day Duration" />
  </div>
    <div class="form-group">
    <label class="control-label">Minutes</label>
    <input type="text" name="minutes" value="<?php echo $row['minutes']; ?>" class="form-control" required placeholder="Enter Minutes Duration" />
  </div>
</div>
<!-- modal footer -->
<div class="modal-footer">
  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
  <button type="submit" class="btn btn-primary btn-icon-split">
    <span class="icon text-white-50"><i class="fa fa-check"></i></span>
    <span class="text">Save</span>
  </button>
</div>
</form>
<?php endforeach; ?>
<script>
  jQuery(document).ready(function() {
    $('form').parsley();
  });
</script>

