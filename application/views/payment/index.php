<?php
    $business_address         =   get_app_config("business_address");
    $system_email             =   get_app_config("system_email");
    $business_phone           =   get_app_config("business_phone");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Abdul Mannan">
    <meta name="copyright" content="Copyright (c) 2014 - 2018 SpaGreen">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
    <link rel="shortcut icon" href="<?php echo base_url('uploads/system_logo/'.get_app_config("favicon")); ?>">
    <!-- CSS-->

    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url(); ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/sb-admin-2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.css" rel="stylesheet" />
    <script src="<?php echo base_url("assets/"); ?>vendor/jquery/jquery.min.js"></script>
</head>

<body id="page-top">
  <div id="wrapper">
    
  </div>


<script src="<?php echo base_url("assets/"); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo base_url("assets/"); ?>vendor/jquery-easing/jquery.easing.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script>

<!--sweet alert2 JS -->
<script src="<?php echo base_url(); ?>assets/js/plugins/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        var success_message = '<?php echo $this->session->flashdata('success'); ?>';
        var error_message = '<?php echo $this->session->flashdata('error'); ?>';
        if (success_message != '') {
            swal('Success!',success_message,'success');
        }
        if (error_message != '') {
            swal('Error!',error_message,'error');
        }
    });
</script>

</body>
</html>