<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ---------------------- Cron Job Intrigation --------------------
 *
 * @author      Rejohn 01552607608
 * @link        ''
 *
 **/
 
class Cron extends CI_Controller{
	function __construct() {
        parent::__construct();
        
    }
    
	public function user_daily_susbscription_status() {
        $result = $this->common_model->user_daily_susbscription_status();
        if(count($result) > 0) {
        	foreach ($result as $key => $value) {
        		// echo $value['email'];
        		// echo "<br/>";
        		// echo $value['name'];
        		// echo "<br/>";
        		// echo $value['end_date'];
        		$this->send_email($value['email'], $value['name'], $value['package_name'], $value['end_date']);
        	}
        }
    }

    public function send_email($to, $name, $pname, $date)
	{
		$this->load->config('email');
        $this->load->library('email');
   
        $from = $this->config->item('smtp_user');
        
        $message = 'Dear '. $name .',<br/> <br/> Your using Packakge <b>'. $pname .'</b> will be expire at <b> '. $date .' </b>. <br/><br/>Thank you.';
       
        $this->email->set_newline("\r\n");
        $this->email->from($from, 'Webbdmeet');
        $this->email->to($to);
        $this->email->subject('Webmeet Subscription Package Expire');
        $this->email->message($message);

        $this->email->send();
	}
}

