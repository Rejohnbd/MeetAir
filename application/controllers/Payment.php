<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * ---------------------- SslCommerz Payment Intrigation --------------------
 *
 * @author      Rejohn 01552607608
 * @link        ''
 *
 **/


class Payment extends CI_Controller { 
	
	public function __construct() {
        parent::__construct();

        $this->load->library('session');
    }

    public function subscription()
    {
    	$this->load->view('payment/checkout');
    }

    public function hosted_payment()
	{
		if($this->input->get_post('placeorder'))
		{
			$post_data = array();
			$post_data['total_amount'] 	= 30;
			$post_data['currency'] 		= "BDT";
			$post_data['tran_id'] 		= "SSLC".uniqid();
			$post_data['success_url'] 	= base_url()."success";
			$post_data['fail_url'] 		= base_url()."fail";
			$post_data['cancel_url'] 	= base_url()."cancel";
			$post_data['ipn_url'] 		= base_url()."ipn";
			# $post_data['multi_card_name'] = "mastercard,visacard,amexcard";  # DISABLE TO DISPLAY ALL AVAILABLE

			# EMI INFO
			// $post_data['emi_option'] = "1";
			// $post_data['emi_max_inst_option'] = "9";
			// $post_data['emi_selected_inst'] = "9";

			# CUSTOMER INFORMATION
			$post_data['cus_name'] 			= $this->input->post('fname')." ".$this->input->post('fname');
			$post_data['cus_email']			= $this->input->post('cus_email');
			$post_data['cus_add1'] 			= 'Test Address';
			$post_data['cus_city'] 			= 'Test City';
			$post_data['cus_country'] 		= 'Bangladesh';
			$post_data['cus_phone'] 		= $this->input->post('cus_phone');
			$post_data['shipping_method'] 	= "NO";
			$post_data['product_name'] 		= "Subscription Package Name";
			$post_data['product_category'] 	= "Subscription Package";
			$post_data['product_profile'] 	= "virtual-package";
			$post_data['num_of_item']		= 3;
			// $post_data['cus_state'] = $this->input->post('state');
			// $post_data['cus_postcode'] = $this->input->post('postcode');

			# SHIPMENT INFORMATION
			// $post_data['ship_name'] 	= $this->input->post('fname')." ".$this->input->post('fname');
			// $post_data['ship_add1'] 	= $this->input->post('add1');
			// $post_data['ship_city'] 	= $this->input->post('state');
			// $post_data['ship_state'] 	= $this->input->post('state');
			// $post_data['ship_postcode']	= $this->input->post('postcode');
			// $post_data['ship_country'] 	= $this->input->post('country');

			# OPTIONAL PARAMETERS
			$post_data['user_id'] 		= 4;
			$post_data['email'] 		= $post_data['cus_email'];
			$post_data['name'] 			= $post_data['cus_name'];
			$post_data['package_id'] 	= 1;
			$post_data['num_of_item'] 	= 3;
			// $post_data['value_a'] = "ref001";
			// $post_data['value_b'] = "ref002";
			// $post_data['value_c'] = "ref003";
			// $post_data['value_d'] = "ref004";

			// $post_data['product_profile'] = "physical-goods";
			// $post_data['shipping_method'] = "NO";
			// $post_data['num_of_item'] = "3";
			// $post_data['product_name'] = "Computer,Speaker";
			// $post_data['product_category'] = "Ecommerce";

			$this->load->library('session');

			$session = array(
				'tran_id' 	=> $post_data['tran_id'],
				'amount' 	=> $post_data['total_amount'],
				'currency'	=> $post_data['currency']
			);

			$this->session->set_userdata('tarndata', $session);

			echo "<pre>";
			// $response = $this->sslcommerz->RequestToSSLC($post_data, SSLCZ_STORE_ID, SSLCZ_STORE_PASSWD);
			// print_r($response);
			print_r($post_data);
			echo "<br>";

			$sesdata = $this->session->userdata('tarndata');
			print_r($sesdata);

			if($this->sslcommerz->RequestToSSLC($post_data, SSLCZ_STORE_ID, SSLCZ_STORE_PASSWD))
			{
				echo "Pending";
				/***************************************
				# Change your database status to Pending.
				****************************************/

				$data['tran_id']        = $post_data['tran_id'];
				$data['user_id']        = 4;
				$data['name']           = $post_data['cus_name'];
				$data['email']          = $post_data['cus_email'];
				$data['phone']          = $post_data['cus_phone'];
				$data['package_id']     = 1;
				$data['num_of_item']    = $post_data['num_of_item'];
            	$data['package_price']	= 300.00;
            	$data['total_amount']   = $post_data['total_amount'];
            	$data['status']     	= 'pending';
            	$data['created_at']     = date("Y-m-d H:i:s");
        
            	$this->db->insert('transaction', $data);
			}
		}
	}

	public function payment_status()
    {
    	$this->load->view('payment/index');		
    }

    public function success_payment()
	{
		/**
	     * Add For Payment Success
	     * @author Rejohn
	     * Need to Add validation
	     */

		$tran_id 	= $this->input->post('tran_id');
		$user_id 	= $this->input->post('value_a');
		$user_email	= $this->input->post('value_b');
		$user_phone = $this->input->post('value_c');
		$package_id = $this->input->post('value_d');

		$this->db->where('tran_id', $tran_id);
		$this->db->where('user_id', $user_id);
		$this->db->where('phone', $user_phone);
		$this->db->where('package_id', $package_id);
		$this->db->where('status', 'pending');
		$query = $this->db->get('transaction');
		$result = $query->row();
		
		$update_data = array(
			'status' 	=> 'success',
			'card_type'	=> $this->input->post('card_type'),
			'ssl_data' 	=> json_encode($this->input->post())
		);

		$this->db->where('id', $result->id);
		$this->db->update('transaction', $update_data);
		$this->db->where('package_id', $result->package_id);
		$query = $this->db->get('package');
		$packageInfo = $query->row();
		$total_days = $result->num_of_item * $packageInfo->duration;

		$subscription_data = array(
			'user_id'		=> $result->user_id,
			'tran_id'		=> $result->id,
			'package_id' 	=> $result->package_id,
			'num_of_item' 	=> $result->num_of_item,
			'package_name' 	=> $result->package_name,
			'package_price'	=> $result->package_price,
			'total_amount' 	=> $result->total_amount,
			'store_amount' 	=> $this->input->post('store_amount'),
			'card_type' 	=> $this->input->post('card_type'),
			'start_date' 	=> date('Y-m-d', strtotime($result->created_at)),
			'end_date' 		=> date('Y-m-d', strtotime($result->created_at . ' +' . $total_days . 'days'))
		);

		$this->db->insert('subscription', $subscription_data);

		$info = array(
			'user_name'		=> $result->name,
			'package_name'	=> $result->package_name,
			'package_price'	=> $result->package_price,
			'start_date'	=> date('Y-m-d', strtotime($result->created_at)),
			'end_date'		=> date('Y-m-d', strtotime($result->created_at . ' +' . $total_days . 'days'))
		);

		$this->send_email($result->email, 'Package Purchase successfully', 'success', $info);
		
		$this->session->set_flashdata('success', 'Payment Successed.');
		redirect(base_url() . 'payment_status');
	}

	public function fail_payment()
	{
		// echo "fail_payment";
		// $database_order_status = 'Pending'; // Check this from your database here Pending is dummy data,
		// if($database_order_status == 'Pending')
		// {
		// 	****************************************************************************
		// 	# Change your database status to FAILED & You can redirect to failed page from here
		// 	*****************************************************************************
		// 	echo "<pre>";
		// 	print_r($_POST);
		// 	echo "Transaction Faild";
		// }
		// else
		// {
		// 	/******************************************************************
		// 	# Just redirect to your success page status already changed by IPN.
		// 	******************************************************************/
		// 	echo "Just redirect to your failed page";
		// }

		/**
	     * Add For Payment Fail
	     * @author Rejohn
	     * Need to Add validation
	     */
		$tran_id 	= $this->input->post('tran_id');
		$user_id 	= $this->input->post('value_a');
		$user_email = $this->input->post('value_b');
		$user_phone = $this->input->post('value_c');
		$package_id = $this->input->post('value_d');

		$this->db->where('tran_id', $tran_id);
		$this->db->where('user_id', $user_id);
		$this->db->where('phone', $user_phone);
		$this->db->where('package_id', $package_id);
		$this->db->where('status', 'pending');
		$query = $this->db->get('transaction');
		$result = $query->row();
		
		$update_data = array(
			'status' 	=> 'fail',
			'card_type'	=> $this->input->post('card_type'),
			'ssl_data' 	=> json_encode($this->input->post())
		);

		$this->db->where('id', $result->id);
		$this->db->update('transaction', $update_data);
		
		$this->session->set_flashdata('error', 'Payment Failed.');
		redirect(base_url() . 'payment_status');
	}

	public function cancel_payment()
	{
		// echo "cancel_payment";
		// $database_order_status = 'Pending'; // Check this from your database here Pending is dummy data,
		// if($database_order_status == 'Pending')
		// {
		// 	****************************************************************************
		// 	# Change your database status to CANCELLED & You can redirect to cancelled page from here
		// 	*****************************************************************************
		// 	echo "<pre>";
		// 	print_r($_POST);
		// 	echo "Transaction Canceled";
		// }
		// else
		// {
		// 	/******************************************************************
		// 	# Just redirect to your cancelled page status already changed by IPN.
		// 	******************************************************************/
		// 	echo "Just redirect to your failed page";
		// }

		/**
	     * Add For Payment Cancel
	     * @author Rejohn
	     * Need to Add validation
	     */
		$tran_id 	= $this->input->post('tran_id');
		$user_id 	= $this->input->post('value_a');
		$user_email = $this->input->post('value_b');
		$user_phone = $this->input->post('value_c');
		$package_id = $this->input->post('value_d');

		$this->db->where('tran_id', $tran_id);
		$this->db->where('user_id', $user_id);
		$this->db->where('phone', $user_phone);
		$this->db->where('package_id', $package_id);
		$this->db->where('status', 'pending');
		$query = $this->db->get('transaction');
		$result = $query->row();
		
		$update_data = array(
			'status' 	=> 'cancel',
			'card_type'	=> $this->input->post('card_type'),
			'ssl_data' 	=> json_encode($this->input->post())
		);

		$this->db->where('id', $result->id);
		$this->db->update('transaction', $update_data);
		
		$this->session->set_flashdata('success', 'Payment Cancel.');
		redirect(base_url() . 'payment_status');
	}

	public function ipn_listener()
	{
		$data['data'] = json_encode($this->input->post());
		$data['created_at'] = now();
		$this->db->insert('ipn', $data);
		
		echo "ipn_listener";
		// $database_order_status = 'Pending'; // Check this from your database here Pending is dummy data,
		// $store_passwd = SSLCZ_STORE_PASSWD;
		// if($ipn = $this->sslcommerz->ipn_request($store_passwd, $_POST))
		// {
		// 	if(($ipn['gateway_return']['status'] == 'VALIDATED' || $ipn['gateway_return']['status'] == 'VALID') && $ipn['ipn_result']['hash_validation_status'] == 'SUCCESS')
		// 	{
		// 		if($database_order_status == 'Pending')
		// 		{
		// 			echo $ipn['gateway_return']['status']."<br>";
		// 			echo $ipn['ipn_result']['hash_validation_status']."<br>";
		// 			/*****************************************************************************
		// 			# Check your database order status, if status = 'Pending' then chang status to 'Processing'.
		// 			******************************************************************************/
		// 		}
		// 	}
		// 	elseif($ipn['gateway_return']['status'] == 'FAILED' && $ipn['ipn_result']['hash_validation_status'] == 'SUCCESS')
		// 	{
		// 		if($database_order_status == 'Pending')
		// 		{
		// 			echo $ipn['gateway_return']['status']."<br>";
		// 			echo $ipn['ipn_result']['hash_validation_status']."<br>";
		// 			****************************************************************************
		// 			# Check your database order status, if status = 'Pending' then chang status to 'FAILED'.
		// 			*****************************************************************************
		// 		}
		// 	}
		// 	elseif ($ipn['gateway_return']['status'] == 'CANCELLED' && $ipn['ipn_result']['hash_validation_status'] == 'SUCCESS') 
		// 	{
		// 		if($database_order_status == 'Pending')
		// 		{
		// 			echo $ipn['gateway_return']['status']."<br>";
		// 			echo $ipn['ipn_result']['hash_validation_status']."<br>";
		// 			/*****************************************************************************
		// 			# Check your database order status, if status = 'Pending' then chang status to 'CANCELLED'.
		// 			******************************************************************************/
		// 		}
		// 	}
		// 	else
		// 	{
		// 		if($database_order_status == 'Pending')
		// 		{
		// 			echo "Order status not ".$ipn['gateway_return']['status'];
		// 			/*****************************************************************************
		// 			# Check your database order status, if status = 'Pending' then chang status to 'FAILED'.
		// 			******************************************************************************/
		// 		}
		// 	}
		// 	echo "<pre>";
		// 	print_r($ipn);
		// }
	}

	public function send_email($to, $subject, $status, $info)
	{
		$this->load->config('email');
        $this->load->library('email');
        
        $from = $this->config->item('smtp_user');
        
        if($status == 'success') {
        	$message = 'Dear '. $info['user_name'] .',<br/> <br/> <b>Package Name: </b> '. $info['package_name'] .' <br/> <b>Package Price: </b> '. $info['package_price'] .' <br/> <b>Start Date: </b> '. $info['start_date'] .' <br/> <b>End Date: </b> '. $info['end_date'] .' <br/><br/>Thank you.';
        }

        $this->email->set_newline("\r\n");
        $this->email->from($from, 'Webbdmeet');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);

        $this->email->send();
        // if ($this->email->send()) {
        //     echo 'Your Email has successfully been sent.';
        // } else {
        //     show_error($this->email->print_debugger());
        // }
	}
}